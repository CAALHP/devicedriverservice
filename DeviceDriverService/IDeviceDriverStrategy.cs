﻿using CAALHP.Contracts;

namespace Plugins.DeviceDriverService
{
    public interface IDeviceDriverStrategy
    {
        bool IsDeviceDriverInstalled(string deviceName, IServiceHostCAALHPContract host);
    }
}